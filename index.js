const express = require("express");
const routes = require("./finances_api/src/routes");

const app = express();
const port = 8080;

app.use(express.json());
app.use(routes);


app.listen(port, () => console.log("Api ouvindo na porta 8080!"));
